package storage

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

const defaultFileMode = 0744

// FileStore keeps the configurations in a file folder, and writes symlinks
// for enabling sites
type FileStore struct {
	configurationDir string
	enabledDir       string
}

// NewFileStore returns a FileStore rooted at the directory specified. Adds a
// trailing slash to the directory if needed.
func NewFileStore(configurationDir, enabledDir string) *FileStore {
	if !strings.HasSuffix(configurationDir, "/") {
		configurationDir = configurationDir + "/"
	}
	if !strings.HasSuffix(enabledDir, "/") {
		enabledDir = enabledDir + "/"
	}
	return &FileStore{
		configurationDir: configurationDir,
		enabledDir:       enabledDir,
	}
}

// Save stores the contents of a file
func (store *FileStore) Save(name string, content []byte) error {
	return ioutil.WriteFile(store.configurationDir+name, content, defaultFileMode)
}

// Load retrieves the contents of a file
func (store *FileStore) Load(name string) ([]byte, error) {
	content, err := ioutil.ReadFile(store.configurationDir + name)
	if err != nil && os.IsNotExist(err) {
		return nil, newNotFoundError(name, store.configurationDir)
	}
	return content, err
}

// List returns a list of all json configuration files stored in the memory store.
func (store *FileStore) List() ([]string, error) {
	files, err := ioutil.ReadDir(store.configurationDir)
	if err != nil {
		return nil, err
	}
	siteNames := make([]string, 0, len(files))
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		if !strings.HasSuffix(file.Name(), ".json") {
			continue
		}
		siteNames = append(siteNames, strings.TrimSuffix(file.Name(), ".json"))
	}
	return siteNames, nil
}

// Enable creates a symbolic link to the enabledDir for the file specified
func (store *FileStore) Enable(name string) error {
	//make sure file exists
	f, err := os.Stat(store.configurationDir + name)
	if err != nil && os.IsNotExist(err) || f == nil || f.IsDir() {
		return newNotFoundError(name, store.configurationDir)
	}
	log.Println("Create link")
	err = os.Symlink(store.configurationDir+name, store.enabledDir+name)
	if err != nil && os.IsExist(err) {
		return nil
	}
	return err
}

// Disable removes the symbolic link in the enabledDir for the file specified
func (store *FileStore) Disable(name string) error {
	err := os.Remove(store.enabledDir + name)
	if err != nil && os.IsNotExist(err) {
		return nil
	}
	return err
}
