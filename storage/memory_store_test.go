package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMemoryStoreSaveLoad(t *testing.T) {
	store := NewMemoryStore()
	name := "testfile1"
	content := []byte("testcontent1")
	err := store.Save(name, content)
	assert.NoError(t, err)

	retrieved, err := store.Load(name)
	assert.NoError(t, err)
	assert.Equal(t, content, retrieved)

	_, err = store.Load("whatever")
	assert.IsType(t, err, &NotFoundError{})
}

func TestMemoryStoreList(t *testing.T) {
	store := NewMemoryStore()
	err := store.Save("1", []byte("1"))
	require.NoError(t, err)
	err = store.Save("1.json", []byte("1.json"))
	require.NoError(t, err)

	err = store.Enable("1")
	require.NoError(t, err)
	assert.Contains(t, store.enabled, "1")

	err = store.Disable("1")
	require.NoError(t, err)
	assert.NotContains(t, store.enabled, "1")
}

func TestMemoryStoreEnableDisable(t *testing.T) {

}
