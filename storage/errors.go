package storage

import "fmt"

// NotFoundError is used to signify a missing file without exposing the full path
// to the end user
type NotFoundError struct {
	fileName string
	fullPath string
}

// newNotFoundError returns a not found error
func newNotFoundError(filename, path string) error {
	return &NotFoundError{fileName: filename, fullPath: path}
}

// Error returns the error string for external users with only the name of the file
func (e *NotFoundError) Error() string {
	return fmt.Sprintf("requested file %s does not exist", e.fileName)
}

// InternalError returns the error string for internal debugging with the
// name and path of the requested file
func (e *NotFoundError) InternalError() string {
	return fmt.Sprintf("file not found: path: %s, filename: %s", e.fullPath, e.fileName)
}
