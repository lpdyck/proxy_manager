package storage

import (
	"strings"
)

// MemoryStore keeps the configurations in memory, mostly for testing
type MemoryStore struct {
	files   map[string][]byte
	enabled map[string]bool
}

// NewMemoryStore returns an in-memory file store
func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		files:   map[string][]byte{},
		enabled: map[string]bool{},
	}
}

// Save saves the content for the specified file to the memory store
func (store *MemoryStore) Save(name string, content []byte) error {
	store.files[name] = content
	return nil
}

// Load retrieves the content for the specified file from the memory store
func (store *MemoryStore) Load(name string) ([]byte, error) {
	c, ok := store.files[name]
	if !ok {
		return nil, newNotFoundError(name, "memory store")
	}
	return c, nil
}

// List returns a list of all json configuration files stored in the memory store.
func (store *MemoryStore) List() ([]string, error) {
	siteNames := make([]string, 0, len(store.files))
	for i := range store.files {
		if strings.HasSuffix(i, ".json") {
			siteNames = append(siteNames, strings.TrimSuffix(i, ".json"))
		}
	}
	return siteNames, nil
}

// Enable adds a site to the enabled map
func (store *MemoryStore) Enable(name string) error {
	_, ok := store.files[name]
	if !ok {
		return newNotFoundError(name, "memory store")
	}
	store.enabled[name] = true
	return nil
}

// Disable removes a site from the enabled map
func (store *MemoryStore) Disable(name string) error {
	delete(store.enabled, name)
	return nil
}
