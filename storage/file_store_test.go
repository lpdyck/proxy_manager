package storage

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFileStoreSaveLoad(t *testing.T) {
	confDir := "../testdata/sites/emptyfolder/"
	enableDir := "../testdata/sites-enabled/"
	store := NewFileStore(confDir[:len(confDir)-1], enableDir[:len(enableDir)-1]) //make sure it will append a slash
	assert.NotNil(t, store)

	for i := 0; i < 5; i++ {
		s := fmt.Sprintf("%d", i)
		err := store.Save(s, []byte(s))
		require.NoError(t, err)
		defer os.Remove(confDir + s)
		s2 := fmt.Sprintf("%d.json", i)
		err = store.Save(s2, []byte(s2))
		defer os.Remove(confDir + s2)
		require.NoError(t, err)
	}

	list, err := store.List()
	assert.NoError(t, err)
	assert.Len(t, list, 5)
}

func TestFileStoreEnableDisable(t *testing.T) {
	confDir := "../testdata/sites/emptyfolder/"
	enabledDir := "../testdata/sites-enabled/"
	store := NewFileStore(confDir, enabledDir)
	assert.NotNil(t, store)

	err := store.Save("1", []byte("1"))
	require.NoError(t, err)
	err = store.Save("1.json", []byte("1.json"))
	require.NoError(t, err)
	defer os.Remove(confDir + "1")
	defer os.Remove(confDir + "1.json")

	err = store.Enable("1")
	require.NoError(t, err)
	_, err = os.Lstat(store.enabledDir + "1")
	assert.NoError(t, err)

	err = store.Disable("1")
	require.NoError(t, err)
	_, err = os.Lstat(store.enabledDir + "1")
	assert.True(t, os.IsNotExist(err), "should not exists but does")
}
