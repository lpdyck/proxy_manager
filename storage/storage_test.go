package storage

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	dir := "../testdata/sites/emptyfolder"
	os.Mkdir(dir, os.ModePerm)
	i := m.Run()
	os.RemoveAll(dir)
	os.Exit(i)
}
