package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"strings"

	"bitbucket.org/lpdyck/proxy_manager/nginx"
	"bitbucket.org/lpdyck/proxy_manager/storage"
)

var configPath string

func init() {
	flag.StringVar(&configPath, "configPath", "./testdata/config.json", "the path for the confguration file")
}

// Configuration contains the main application configuration
type Configuration struct {
	DataStore               string `json:"data_store"`
	SitesAvailableDirectory string `json:"sites_available_directory"`
	SitesEnabledDirectory   string `json:"sites_enabled_directory"`
	BindPort                string `json:"bind_port"`
}

func loadConfigFromFile(path string) (*Configuration, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	config := new(Configuration)
	err = json.Unmarshal(b, config)
	if err != nil {
		return nil, err
	}
	if !strings.HasSuffix(config.SitesAvailableDirectory, "/") {
		config.SitesAvailableDirectory += "/"
	}

	if !strings.HasSuffix(config.SitesEnabledDirectory, "/") {
		config.SitesEnabledDirectory += "/"
	}
	return config, nil
}

func setupDataStore(conf *Configuration) nginx.Storage {
	switch conf.DataStore {
	case "file":
		return storage.NewFileStore(conf.SitesAvailableDirectory, conf.SitesEnabledDirectory)
	case "memory":
		return storage.NewMemoryStore()
	default:
		panic("data_store parameter required, only allows \"file\" or \"memory\"")
	}
}
