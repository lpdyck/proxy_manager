package nginx

// Storage is an interface describing a way to store configurations
type Storage interface {
	Save(name string, content []byte) error
	Load(name string) (content []byte, err error)
	List() ([]string, error)
	Enable(name string) error
	Disable(name string) error
}
