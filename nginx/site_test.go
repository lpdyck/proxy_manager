package nginx

import (
	"testing"

	"bitbucket.org/lpdyck/proxy_manager/storage"
	"github.com/stretchr/testify/assert"
)

const testConfigPath = `../testdata/sites/`

func TestReadWriteSite(t *testing.T) {
	store := storage.NewMemoryStore()
	//create sample configuration
	site := NewSite("06-blank-config")
	err := site.Save(store)
	assert.Nil(t, err)

	site2 := NewSite("06-blank-config")
	err = site.Load(store)
	assert.Nil(t, err)

	assert.Equal(t, site, site2)
}
