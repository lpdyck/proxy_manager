package nginx

import (
	"io/ioutil"
	"strings"
)

const nginxSitesDir = "../testdata/sites/"

// GetNginxSiteNames gets a map of the sites available
func GetNginxSiteNames(siteDir string) ([]string, error) {
	files, err := ioutil.ReadDir(siteDir)
	if err != nil {
		return nil, err
	}
	siteNames := make([]string, 0, len(files))
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		if !strings.Contains(file.Name(), ".json") {
			continue
		}
		siteNames = append(siteNames, file.Name())
	}
	return siteNames, nil
}
