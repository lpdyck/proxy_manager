package nginx

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"
)

const (
	serverBlockTemplate = `server {
	server_name %[1]s;` // [1] space-separated server names list

	listenBlockTemplate = `
	listen %[1]s %[2]s;` // [1] port [2] defaultServerString || empty string

	locationBlockTemplate = `
	location %[1]s {
		proxy_set_header X-Real-IP  $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header Host $host;
		proxy_pass %[2]s;
	}` // [1] relative location path [2] backend server url

	defaultServerString = "default_server"
)

//Site contains the information for an Nginx configuration site
type Site struct {
	Name    string `json:"site_name"` //name of the site file
	IsNew   bool   `json:"-"`
	Enabled bool   `json:"enabled"`
	Server  Server `json:"server"`
}

// Server contains the information for a server
type Server struct {
	ServerName []string   `json:"server_names"`
	Listens    []Listen   `json:"listen"`
	Locations  []Location `json:"locations"`
}

// Listen contains the configuration for listen addresses
type Listen struct {
	Address       string `json:"address"`
	DefaultServer bool   `json:"default"`
}

// Location contains the configuration for one location
type Location struct {
	Path          string `json:"path"`
	BackendServer string `json:"backend_server"`
}

// NewSite creates a new configuration object with the SiteName specified
func NewSite(siteName string) *Site {
	return &Site{Name: siteName}
}

//NewSiteFromJSON is used to parse a site that is in a json-encoded []byte
func NewSiteFromJSON(payload []byte) (*Site, error) {
	site := new(Site)
	err := json.Unmarshal(payload, site)
	return site, err
}

// Save writes the configuration to the JSON file for storage. The JSON file is
// stored in the configuration folder with the name <SiteName>.json
func (s *Site) Save(store Storage) error {
	content, err := json.Marshal(s)
	if err != nil {
		return err
	}
	err = store.Save(s.Name+".json", content)
	if err != nil {
		return err
	}
	err = s.writeConfig(store)
	if err != nil {
		return err
	}
	if s.Enabled {
		err = store.Enable(s.Name)
		if err != nil {
			return err
		}
	} else {
		err = store.Disable(s.Name)
		if err != nil {
			return err
		}
	}
	return nil
}

// Load reads the configuration from the JSON file.
func (s *Site) Load(store Storage) error {
	content, err := store.Load(s.Name + ".json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(content, s)
	return err
}

func (s *Site) writeConfig(store Storage) error {
	b := bytes.NewBuffer(nil)
	s.Server.writeConfig(b)
	return store.Save(s.Name, b.Bytes())
}

func (s Server) writeConfig(w io.Writer) {
	w.Write([]byte(fmt.Sprintf(serverBlockTemplate, strings.Join(s.ServerName, " "))))
	for _, l := range s.Listens {
		l.writeConfig(w)
	}
	for _, l := range s.Locations {
		l.writeConfig(w)
	}
	w.Write([]byte("}"))
}

func (l Listen) writeConfig(w io.Writer) {
	ds := ""
	if l.DefaultServer {
		ds = defaultServerString
	}
	w.Write([]byte(fmt.Sprintf(listenBlockTemplate, l.Address, ds)))
}

func (l Location) writeConfig(w io.Writer) {
	w.Write([]byte(fmt.Sprintf(locationBlockTemplate, l.Path, l.BackendServer)))
}
