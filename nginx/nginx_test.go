package nginx

import (
	"testing"

	"bitbucket.org/lpdyck/proxy_manager/storage"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetNginxSiteNames(t *testing.T) {
	siteNames, err := GetNginxSiteNames(nginxSitesDir)
	require.Nil(t, err)
	for _, v := range siteNames {
		assert.Contains(t, v, ".json")
	}
	assert.Contains(t, siteNames, "01-default.json")
}

func TestWriteNginxConfig(t *testing.T) {
	store := storage.NewMemoryStore()
	site := &Site{
		Name:    "test_write_config",
		Enabled: true,
		Server: Server{
			ServerName: []string{"test.write.config"},
			Listens: []Listen{{
				Address:       "80",
				DefaultServer: false}, {
				Address:       "8080",
				DefaultServer: false},
			},
			Locations: []Location{{
				Path:          "/",
				BackendServer: "http://127.0.0.1:8002"}, {
				Path:          "/other",
				BackendServer: "http://127.0.0.1:8003"},
			},
		},
	}
	err := site.writeConfig(store)
	assert.NoError(t, err)
	content, err := store.Load(site.Name)
	require.NoError(t, err)
	assert.Equal(t, siteConfigExpected, string(content))

}

const siteConfigExpected = `server {
	server_name test.write.config;
	listen 80 ;
	listen 8080 ;
	location / {
		proxy_set_header X-Real-IP  $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header Host $host;
		proxy_pass http://127.0.0.1:8002;
	}
	location /other {
		proxy_set_header X-Real-IP  $remote_addr;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header Host $host;
		proxy_pass http://127.0.0.1:8003;
	}}`
