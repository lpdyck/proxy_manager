package templates

const listHTML = `<!DOCTYPE html>
<html>
<head>
  <title>Reverse Proxy Manager - List Sites</title>
</head>
<body>
  {{range $name := .}}
  <div><a href="/edit.html?site={{$name}}">{{$name}}</a></div>
  {{else}}
  <p>No Sites Configured</p>
  {{end}}
  <div><a href="/edit.html?site=new">Create a New Site</a></div>
</body>
</html>`
