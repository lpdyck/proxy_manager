package templates

import "html/template"

var allTemplates = map[string]string{
	"edit.html":           editHTML,
	"list.html":           listHTML,
	"serverNameSnippet":   serverNameSnippet,
	"jsserverNameSnippet": template.JSEscapeString(serverNameSnippet),
	"listenSnippet":       listenSnippet,
	"jslistenSnippet":     template.JSEscapeString(listenSnippet),
	"locationSnippet":     locationSnippet,
	"jslocationSnippet":   template.JSEscapeString(locationSnippet),
}

//Load loads the web templates
func Load() *template.Template {
	templates := template.New("")
	for i, v := range allTemplates {
		template.Must(templates.New(i).Parse(v))
	}
	return templates
}
