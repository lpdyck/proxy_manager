package templates

const (
	serverNameSnippet = `
<div class="server_name_block">
	<span title="Enter the server name for this site, for example www.example.com or example.com. Using _ will make this a catch-all server">
		<label>Server Name</label>
		<input type="text" name="server_name" placeholder="www.example.com" value="{{.}}" required>
	</span>
</div>`

	listenSnippet = `
<div class="listen_block">
	<span title="Enter the address to listen to for this site. 0.0.0.0 listens to any address, 127.0.0.1 listens only to the local computer. You can specify a port by appending : and the port number.">
		<label>Address</label>
		<input type="text" name="address" placeholder="127.0.0.1" value="{{.Address}}" required>
	</span>
	<span title="Shoud this server receive all requests on this address not already handled by another server. This should only be enabled on one server at a time">
		<input type="checkbox" name="default_server" {{if .DefaultServer}}checked{{end}}>Default Server?
	</span>
</div>`

	locationSnippet = `
<div class="location_block">
	<div>
		<span title="Put the relative path for this location. / will handle requests at the root of the domain, and any path can be appended to that.">
			<label>Path</label>
			<input type="text" name="path" placeholder="/" value="{{.Path}}" required>
		</span>
	</div>
	<div>
		<span title="Put the backend server to which the requests should be forwarded. Include http:// or https://">
			<label>Backend Server</label>
			<input type="text" name="backend_server" placeholder="http://127.0.0.1:8000" value="{{.BackendServer}}" required>
		</span>
	</div>
</div>`
)
