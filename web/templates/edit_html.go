package templates

const editHTML = `<!DOCTYPE html>
<html>
<head>
  <title>Reverse Proxy Manager - Edit {{.Name}}</title>
</head>
<body>
  <div><form id="edit_form">
    <div>
      <span title="Enter a descriptive name for your site so you can find it later. This can be the same as the server_name or something more colloquial">
        <label>Site Name</label>
        <input type="text" name="site_name" placeholder="My New Site" required {{if not .IsNew}}disabled value="{{.Name}}"{{end}}>
      </span>
      <span title="Should this site be enabled for processing requests?">
        <input type="checkbox" name="enabled" {{if .Enabled}}checked{{end}}>Enabled?
      </span>
    </div>

    {{with .Server}}
    <div id="server_name_blocks">
    {{range .ServerName}}
      {{template "serverNameSnippet" .}}
    {{else}}
      {{template "serverNameSnippet"}}
    {{end}}
    <button type="button" id="add_a_server">Add Server Name</button>
    </div>

    <div id="listen_blocks">
    {{range .Listens}}
      {{template "listenSnippet" .}}
    {{else}}
      {{template "listenSnippet"}}
    {{end}}
    <button type="button"  id="add_a_listen">Add Listen</button>
    </div>

    <div id="location_blocks">
    {{range .Locations}}
      {{template "locationSnippet" .}}
    {{else}}
      {{template "locationSnippet"}}
    {{end}}
    <button type="button"  id="add_a_location">Add Location</button>
    <div>
    {{end}}

    <button type="button" id="save">Save</button>
  </form></div>
  <a href="/list.html">Back to List</a>
</body>
<script>var serverNameSnippet = "{{template "jsserverNameSnippet"}}"
var listenSnippet = "{{template "jslistenSnippet"}}"
var locationSnippet = "{{template "jslocationSnippet"}}"
</script>
<script src="/static/edit.js"></script>
</html>`
