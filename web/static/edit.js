function addNewBlock(buttonName, newHTML) {
	return function(){
	this.insertAdjacentHTML("beforebegin",newHTML)
	}
}

function getServerNames() {
	var serverNames = []
	serverNameBlocks = document.getElementsByName("server_name");
	for (i=0;i<serverNameBlocks.length;i++) {
		serverNames.push(serverNameBlocks[i].value);
	}
	return serverNames
}

function getListens() {
	listenBlocks = document.getElementsByClassName("listen_block");
	listenObjs = new Array();
	for (i = 0; i < listenBlocks.length;i++){
		listen = new Object();
		listen.address = listenBlocks[i].querySelector('input[name="address"]').value;
		listen.default = listenBlocks[i].querySelector('input[name="default_server"]').checked;
		listenObjs.push(listen);
	}
	return listenObjs
}

function getLocations() {
	locationBlocks = document.getElementsByClassName("location_block");
	locationObjs = new Array();
	for (i = 0; i < locationBlocks.length; i++) {
		loc = new Object();
		loc.path = locationBlocks[i].querySelector('input[name="path"]').value;
		loc.backend_server = locationBlocks[i].querySelector('input[name="backend_server"]').value
		locationObjs.push(loc);
	}
	return locationObjs
}

function doPostForm(text) {
	var f = document.createElement("form");
	f.setAttribute('method',"post");
	f.setAttribute('hidden',true);

	var i = document.createElement("input"); //input element, text
	i.setAttribute('type',"text");
	i.setAttribute('name',"json");
	i.setAttribute('value',text);
	f.appendChild(i);
	document.body.appendChild(f);
	f.submit()
}

function submitForm() {
	if (!document.forms[0].reportValidity()) {
		return;
	}
	var site = new Object();
	site.site_name = document.getElementsByName("site_name")[0].value;
	site.enabled = document.getElementsByName("enabled")[0].checked;
	site.server = new Object();
	site.server.server_names = getServerNames();
	site.server.listen = getListens();
	site.server.locations = getLocations();

	var jsonString= JSON.stringify(site);
	console.log(jsonString);
	
	doPostForm(jsonString);
}

document.getElementById("add_a_server").addEventListener("click", addNewBlock(this,serverNameSnippet))
document.getElementById("add_a_listen").addEventListener("click", addNewBlock(this,listenSnippet))
document.getElementById("add_a_location").addEventListener("click", addNewBlock(this,locationSnippet))
document.getElementById("save").addEventListener("click",submitForm)



/*
{
	"site_name":"",
	"enabled":true,
	"server":{
		"server_names":[""],
		"listen":[
			{
				"address":"",
				"default":false
			}
		],
		"locations":[
			{
				"path":"",
				"backend_server":""
			}
		]
	}
}
*/