package web

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// File names should only contains letters, numbers, _, or -
func TestValidateSiteName(t *testing.T) {
	fileNames := map[string]bool{
		"valid1":   true,
		"valid_2":  true,
		"valid-3":  true,
		"invalid*": false,
		"invalid.": false,
		"invalid(": false,
		"invalid^": false,
	}

	for text, pass := range fileNames {
		assert.Equal(t, pass, validateSiteName(text))
	}
}
