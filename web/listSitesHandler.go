package web

import (
	"html/template"
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/nginx"
)

// ListSitesHandler reads and returns the sites available for an nginx instance
type ListSitesHandler struct {
	store     nginx.Storage
	templates *template.Template
	sites     []string
}

func (h ListSitesHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	h.sites, err = h.store.List()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := h.templates.ExecuteTemplate(w, "list.html", h.sites); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
