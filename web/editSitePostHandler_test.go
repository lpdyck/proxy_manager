package web

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"bitbucket.org/lpdyck/proxy_manager/storage"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestEditSitePostHandlerOverwritesFile(t *testing.T) {
	store := storage.NewMemoryStore()
	s := httptest.NewServer(SetupRouter(store))
	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	name := "03-overwrite"
	store.Save(name, []byte("{}"))
	path := "/edit.html?site=03-overwrite"
	form := url.Values{}
	content := fmt.Sprintf(`{"site_name":%q}`, name)
	form.Add("json", content)
	resp, err := client.PostForm(s.URL+path, form)
	require.Nil(t, err)
	assert.Equal(t, http.StatusSeeOther, resp.StatusCode, readRespBody(t, resp.Body))
	assert.Equal(t, path, resp.Header.Get("location"))
	require.Nil(t, err)

	b, err := store.Load(name + ".json")
	require.Nil(t, err)
	expected := "{\"site_name\":\"03-overwrite\",\"enabled\":false,\"server\":{\"server_names\":null,\"listen\":null,\"locations\":null}}"
	assert.Equal(t, expected, string(b))
}

func TestEditSitePostHandlerCreatesNewFileWhenSiteEqualsNew(t *testing.T) {
	store := storage.NewMemoryStore()
	s := httptest.NewServer(SetupRouter(store))
	client := http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	name := "testNewSite"
	path := "/edit.html?site=new"
	form := url.Values{}
	form.Add("json", fmt.Sprintf(`{"site_name":%q}`, name))
	resp, err := client.PostForm(s.URL+path, form)
	require.Nil(t, err)
	assert.Equal(t, http.StatusSeeOther, resp.StatusCode, readRespBody(t, resp.Body))
	assert.Equal(t, "/edit.html?site=testNewSite", resp.Header.Get("location"))
	require.Nil(t, err)

	resp, err = http.Get(s.URL + "/edit.html?site=testNewSite")
	require.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode, readRespBody(t, resp.Body))
}
