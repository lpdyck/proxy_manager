package web

import (
	"log"
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/storage"
)

// NotFoundError writes a detailed error to the log and writes a less detailed
// error to the client
func NotFoundError(w http.ResponseWriter, err *storage.NotFoundError) {
	log.Println(err.InternalError())
	http.Error(w, err.Error(), http.StatusNotFound)
}

// UnknownError writes unknown errors to the log and writes a generic error
// message to the client
func UnknownError(w http.ResponseWriter, err error) {
	log.Println(err)
	http.Error(w, "an unknown error occurred", http.StatusInternalServerError)
}
