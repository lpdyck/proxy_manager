package web

import (
	"io"
	"io/ioutil"
	"testing"
)

func readRespBody(t *testing.T, r io.Reader) string {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		t.Fatal(err)
	}
	return string(b)
}
