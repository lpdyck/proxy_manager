package web

import (
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/nginx"
	"bitbucket.org/lpdyck/proxy_manager/storage"
)

// EditSiteHandler reads and returns the sites available for an nginx instance
type EditSiteHandler struct {
	store     nginx.Storage
	templates *template.Template
	site      *nginx.Site
}

func (h EditSiteHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	siteName := r.URL.Query().Get("site")
	if siteName == "" {
		http.Error(w, "You must specify a site name", http.StatusBadRequest)
		return
	}

	if !validateSiteName(siteName) {
		http.Error(w, "site name can only contain letters, numbers, _, or -", http.StatusBadRequest)
		return
	}

	h.site = &nginx.Site{
		Name:  siteName,
		IsNew: true,
	}
	if siteName != "new" {
		h.site.IsNew = false
		err := h.site.Load(h.store)
		switch err.(type) {
		case *storage.NotFoundError:
			NotFoundError(w, err.(*storage.NotFoundError))
			return
		case nil:
		default:
			UnknownError(w, err)
			return
		}
	}

	if err := h.templates.ExecuteTemplate(w, "edit.html", h.site); err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
