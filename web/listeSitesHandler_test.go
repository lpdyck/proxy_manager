package web

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/lpdyck/proxy_manager/storage"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestListSitesHandlerReturnsStatusOK(t *testing.T) {
	store := storage.NewMemoryStore()
	s := httptest.NewServer(SetupRouter(store))
	resp, err := http.Get(s.URL + "/list.html")
	require.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode, readRespBody(t, resp.Body))
}

func TestListSitesHandlerReturnsSites(t *testing.T) {
	store := storage.NewMemoryStore()
	store.Save("01-default.json", []byte("{}"))
	store.Save("02-dummy.json", []byte("{}"))
	s := httptest.NewServer(SetupRouter(store))
	resp, err := http.Get(s.URL + "/list.html")
	require.Nil(t, err)
	body := readRespBody(t, resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode, body)
	assert.Contains(t, body, `<a href="/edit.html?site=01-default">01-default</a>`)
	assert.Contains(t, body, `<a href="/edit.html?site=02-dummy">02-dummy</a>`)
}
