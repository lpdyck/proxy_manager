package web

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/lpdyck/proxy_manager/storage"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestEditSiteHandlerRequiresValidSiteName(t *testing.T) {
	store := storage.NewMemoryStore()
	store.Save("01-default.json", []byte("{}"))
	s := httptest.NewServer(SetupRouter(store))
	t.Run("no site query", func(t *testing.T) {
		resp, err := http.Get(s.URL + "/edit.html")
		require.Nil(t, err)
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode, readRespBody(t, resp.Body))
	})

	t.Run("no site name", func(t *testing.T) {
		resp, err := http.Get(s.URL + "/edit.html?site=")
		require.Nil(t, err)
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode, readRespBody(t, resp.Body))
	})

	t.Run("invalid site name", func(t *testing.T) {
		resp, err := http.Get(s.URL + "/edit.html?site=notarealsite")
		require.Nil(t, err)
		assert.Equal(t, http.StatusNotFound, resp.StatusCode, readRespBody(t, resp.Body))
	})

	t.Run("site name contains invalid characters", func(t *testing.T) {
		resp, err := http.Get(s.URL + "/edit.html?site=thisFileContains.*")
		require.Nil(t, err)
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode, readRespBody(t, resp.Body))
	})

	t.Run("valid site name", func(t *testing.T) {
		resp, err := http.Get(s.URL + "/edit.html?site=01-default")
		require.Nil(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode, readRespBody(t, resp.Body))
	})
}

func TestEditSiteHandlerNewSiteReturnsEditableInputField(t *testing.T) {
	store := storage.NewMemoryStore()
	store.Save("01-default.json", []byte("{}"))
	s := httptest.NewServer(SetupRouter(store))
	resp, err := http.Get(s.URL + "/edit.html?site=new")
	require.Nil(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	d, err := goquery.NewDocumentFromResponse(resp)
	_, ok := d.Find("input[type=text]").Attr("disabled")
	assert.False(t, ok)

	resp, err = http.Get(s.URL + "/edit.html?site=01-default")
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	require.Nil(t, err)
	d, err = goquery.NewDocumentFromResponse(resp)
	_, ok = d.Find("input[type=text]").Attr("disabled")
	assert.True(t, ok)
}
