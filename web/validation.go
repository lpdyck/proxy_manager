package web

import "unicode"

func validateSiteName(name string) bool {
	for _, char := range name {
		if !unicode.IsLetter(char) && !unicode.IsNumber(char) && char != '_' && char != '-' {
			return false
		}
	}
	return true
}
