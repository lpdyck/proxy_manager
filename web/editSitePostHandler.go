package web

import (
	"log"
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/nginx"
)

//EditSitePostHandler writes to config files from the posted form
type EditSitePostHandler struct {
	store    nginx.Storage
	siteName string
}

func (h EditSitePostHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	jsonString := r.FormValue("json")
	site, err := nginx.NewSiteFromJSON([]byte(jsonString))
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if !validateSiteName(site.Name) {
		log.Println(err)
		http.Error(w, "site name can only contain letters, numbers, _, or -", http.StatusBadRequest)
		return
	}

	err = site.Save(h.store)
	if err != nil {
		log.Println(err)
		UnknownError(w, err)
		return
	}

	w.Header().Set("location", "/edit.html?site="+site.Name)
	http.Error(w, "", http.StatusSeeOther)
}
