package web

import (
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/nginx"
	"bitbucket.org/lpdyck/proxy_manager/web/templates"

	"github.com/gorilla/mux"
)

// SetupRouter parses templates and registers the routes for the web server
func SetupRouter(store nginx.Storage) *mux.Router {
	templates := templates.Load()
	router := mux.NewRouter()

	router.Handle("/list.html", ListSitesHandler{templates: templates, store: store})
	router.Handle("/edit.html", EditSitePostHandler{store: store}).Methods(http.MethodPost)
	router.Handle("/edit.html", EditSiteHandler{templates: templates, store: store}).Methods(http.MethodGet)
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("web/static"))))
	router.Handle("/", http.RedirectHandler("/list.html", http.StatusMovedPermanently))
	return router
}
