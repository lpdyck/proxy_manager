## Purpose

Proxy_Manager is a simple graphic tool for setting up and managing a reverse proxy usinvg nginx.

## Reasons For Existence

In an office I worked in, there was an IIS server that was doing almost nothing except for serving as a reverse proxy to backend servers.
The server was, of course, a resource hog for the exxtremely simple task it was performing and didn't really support things like web sockets.
I wrote this application to make the nginx config accessible for people who didn't want to memorize nginx config file conventions, just set up a server.

## Dependencies

Proxy_Manager requires
 - go 1.10 or higher (you may be able to get away with older, but why bother?)
 - make
It doesn't actually require nginx, it will store the configs wherever you tell it to, and as of yet does not actually reload nginx

## Test

Run `make test` to test the application.

## Build

Run `make` to build the appllication

## Install

No installation is configured yet. It will come.