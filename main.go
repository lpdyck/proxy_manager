package main

import (
	"flag"
	"log"
	"net/http"

	"bitbucket.org/lpdyck/proxy_manager/web"
)

func main() {
	flag.Parse()
	conf, err := loadConfigFromFile(configPath)
	if err != nil {
		log.Println(err)
		return
	}
	store := setupDataStore(conf)
	log.Println("Reading from", conf.SitesAvailableDirectory, "and", conf.SitesEnabledDirectory)
	http.Handle("/", web.SetupRouter(store))
	log.Println("Server listening on", conf.BindPort)
	http.ListenAndServe(conf.BindPort, nil)
}
